<?php

$date_tomorrow = date('Y-m-d');
$visit_date = date('jS M Y',strtotime($date_tomorrow));

$date_tomorrow = date('Y-m-d');
$date_tomorrow = date("Y-m-d", strtotime("-1 day", strtotime($date_tomorrow)));
// echo $date_tomorrow;

$branch = $this->config->item('branch_name');
$message['subject'] =  $branch.' '.$visit_date.' report';

$this->db->where('shift_type = 0');
$this->db->order_by('shift_id','DESC');
$this->db->limit(1);
$query = $this->db->get('personnel_shift');
$shifts_results = '';
if($query->num_rows() > 0)
{
	$total_shift_balance = 0;
	foreach ($query->result() as $key => $value) {
		# code...
		$time = $time_closing = $value->time;
		$amount = $value->amount;
		$mpesa_amount = $value->mpesa_amount;
		$expense_amount = $value->expense_amount;
		$created = $date_closing = $value->created;
		$closing_date = date('jS M Y',strtotime($created));
		$closing_time = date('H:i:s A',strtotime($time));
		$total_shift_balance = $amount;
	}

	$shifts_results .= '
							<p><strong>Closing Date & Time :</strong> '.$closing_date.' '.$closing_time.'</p>
							<p><strong>Cash Collected (Expenses Inc) : </strong> '.number_format($amount+$expense_amount,2).'</p>
							<p><strong>Mpesa Collected : </strong> '.number_format($mpesa_amount,2).'</p>
							<p><strong>Expensed Amount : </strong> '.number_format($expense_amount,2).'</p>
							<p><strong>Shift Balance (Expense Excl) : </strong> '.number_format($total_shift_balance,2).'</p>
						';
}





$this->db->where('shift_type = 1');
$this->db->order_by('shift_id','DESC');
$this->db->limit(1);
$query_opening = $this->db->get('personnel_shift');
$opening_result = '';
if($query_opening->num_rows() > 0)
{  
	$total_opening_shift_balance = 0;
	foreach ($query_opening->result() as $key => $value_opening) {
		# code...
		$time = $time_opening = $value_opening->time;
		$amount = $value_opening->amount;
		$mpesa_amount = $value_opening->mpesa_amount;
		$expense_amount = $value_opening->expense_amount;
		$created = $date_opening = $value_opening->created;
		$opening_date = date('jS M Y',strtotime($created));
		$opening_time  = date('H:i:s A',strtotime($time));
		$total_opening_shift_balance = $mpesa_amount+ $amount - $expense_amount;
	}

	$opening_result .= '
							<p><strong>Opening Date & Time :</strong> '.$opening_date.' '.$opening_time.'</p>
							<p><strong>Cash Collected : </strong> '.number_format($amount,2).'</p>
							<p><strong>Mpesa Collected : </strong> '.number_format($mpesa_amount,2).'</p>
							<p><strong>Expensed Amount : </strong> '.number_format($expense_amount,2).'</p>
							<p><strong>Shift Balance : </strong> '.number_format($total_opening_shift_balance,2).'</p>
						';
}

// $opening_result .= '<h4 style="text-decoration:underline">Total Collection</h4>
// 							<p><strong>Total Collection: </strong> '.number_format($total_shift_balance + $total_opening_shift_balance,2).'</p>
// 						';
	// var_dump($time_opening); die();
//cash payments
$where = $where1 = $where6 = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_date = "'.$date_opening.'"';
$payments_where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 ';
$table = 'visit, patients';
$where2 = $payments_where.' AND payments.payment_method_id = 2 AND payments.payment_type = 1 AND payments.cancel = 0 AND payments.created BETWEEN  "'.$date_opening.' '.$time_opening.'" AND "'.$date_closing.' '.$time_closing.'" ';
$total_cash_collection = $this->reports_model->get_total_cash_collection($where2, $table);

// mpesa
$where2 = $payments_where.' AND payments.payment_method_id = 5 AND payments.payment_type = 1 AND payments.cancel = 0 AND payments.created BETWEEN  "'.$date_opening.' '.$time_opening.'" AND "'.$date_closing.' '.$time_closing.'" ';
$total_mpesa_collection = $this->reports_model->get_total_cash_collection($where2, $table);

$where2 = $payments_where.' AND (payments.payment_method_id = 1 OR  payments.payment_method_id = 6 OR  payments.payment_method_id = 7 OR  payments.payment_method_id = 8)  AND payments.payment_type = 1 AND payments.cancel = 0 AND payments.created BETWEEN  "'.$date_opening.' '.$time_opening.'" AND "'.$date_closing.' '.$time_closing.'" ';
$total_other_collection = $this->reports_model->get_total_cash_collection($where2, $table);


$where4 = 'payments.payment_method_id = payment_method.payment_method_id AND payments.visit_id = visit.visit_id  AND visit.visit_delete = 0  AND visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND payments.cancel = 0 AND payments.payment_type = 2 AND payments.created BETWEEN  "'.$date_opening.' '.$time_opening.'" AND "'.$date_closing.' '.$time_closing.'" ';
$total_waiver = $this->reports_model->get_total_cash_collection($where4, 'payments, visit, patients, visit_type, payment_method', 'cash');	

echo '<p>Good evening to you,<br>
		Herein is a report of shift transactions. This is sent at '.date('H:i:s A').'
		</p>
		<h4 style="text-decoration:underline"><strong>OPENING VS CLOSING SHIFT SUMMARY</strong></h4>
		<table  class="table table-hover table-bordered ">
			<thead>
				<tr>					
					<th style="padding:5px;">Shift Opening Summary Report</th>
					<th style="padding:5px;">Shift Closed Summary Report</th>
				</tr>
			</thead>
			<tbody>
				<tr>

					<td style="padding:10px;">'.$opening_result.'</td>
					<td style="padding:10px;">'.$shifts_results.'</td>
				</tr>
			</tbody>
		</table>
		

		<h4 style="text-decoration:underline"><strong>COLLECTIONS SUMMARY</strong></h4>
		<table  class="table table-hover table-bordered ">
				<thead>
					<tr>
						<th width="50%"></th>
						<th width="50%"></th>
					</tr>
				</thead>
				</tbody>
		  	<tr>
		  		<td>CASH COLLECTED </td><td>KES. '.number_format($total_cash_collection,2).'</td>
		  	</tr>
		  	<tr>
		  		<td>MPESA COLLECTED </td><td> KES. '.number_format($total_mpesa_collection,2).'</td>
		  	</tr>
		  	<tr>
		  		<td>OTHER COLLECTION</td><td> KES. '.number_format($total_other_collection,2).'</td>
		  	</tr>
		  	
		  	<tr>
		  		<td>CASH - PETTY CASH TRANSFER </td><td> ( KES. '.number_format($total_transfers,2).' )</td>
		  	</tr>
		  	<tr>
		  		<td><strong>REVENUE</strong> </td><td><strong> KES. '.number_format(($total_mpesa_collection + $total_cash_collection + $total_other_collection) - $total_transfers,2).' </strong></td>
		  	</tr>
		  	<tr>
		  		<td><strong>WAIVERS</strong> </td><td> KES. '.number_format($total_waiver,2).'</td>
		  	</tr>
		  	</tbody>

		</table>
		';
?>