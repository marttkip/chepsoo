<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$visit_id = $patient['visit_id'];
$gender = $patient['gender'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
$credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
$debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);





//services details
$item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items_receipt($visit_id);
$credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
$debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);

//payments
$payments_rs = $this->accounts_model->payment_detail_visit($receipt_payment_id,$visit_id);

// var_dump($payments_rs); die();
$total_payments = 0;
					
if(count($payments_rs) > 0)
{
	$x=0;
	
	foreach ($payments_rs as $key_items):
		$x++;
        $payment_type = $key_items->payment_type;
        $payment_method = $key_items->payment_method;
       


	endforeach;
    
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?php echo $contacts['company_name'];?> | Receipt</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
		body{
				font-family:"Times New Roman", Times,Serif, monospace;
				letter-spacing:1px; 
				font-size: 13px;
				font-weight: 20px;
			}
        .receipt_spacing{}
        .center-align{margin:0 auto; text-align:center;}
        
        .receipt_bottom_border{border-bottom: #888888 medium solid;}
        .row .col-md-12 table {
            border:solid #000 !important;
            border-width:1px 0 0 1px !important;
        }
        .row .col-md-12 th, .row .col-md-12 td {
            border:solid #000 !important;
            border-width:0 1px 1px 0 !important;
        }
        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
        {
             padding:4px;
        }
        
        .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
        .title-img{float:left; padding-left:30px;}
		img.logo{max-height:70px; margin:0 auto;}
    </style>
    </head>
    <body class="receipt_spacing" onLoad="window.print();return false;">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align ">
              <strong>
                    <?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
                </strong>
            </div>
        </div>
        
        
        <!-- Patient Details -->
    	<div class="row receipt_bottom_border" style="margin-bottom: 10px; margin-top: 20px; text-align: left">
        
        		<div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">RECEIPT: </div>                        
                    	 <strong><?php echo $receipt_payment_id; ?>, <?php echo $visit_id; ?></strong>
                    </div>
                </div>
            	<div class="row">
                	<div class="col-md-12">
                    	
                    	<div class="title-item">MEMBER :</div>
                        
                    	<strong><?php echo strtoupper($patient_surname.' '.$patient_othernames); ?></strong>
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
                    	
                    	<div class="title-item">DATE :</div>
                        
                    	<strong><?php echo $visit_date.' '.date('H:i:s'); ?></strong>
                    </div>
                </div>
            	
            
        </div>
        
    	<div class="row receipt_bottom_border">
        	<div class="col-md-12">
            	<table class="table table-hover table-bordered table-striped col-md-12">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Service</th>
                      <th>Qty</th>
                      <th>Unit Cost</th>
                      <th>VAT</th>
                      <th>Total Cost</th>
                    </tr>
                    </thead>
                    <tbody>
						<?php
                        $total = 0;
                        $total_vat = 0;
                        $vat = 0;
                        if($item_invoiced_rs->num_rows() > 0){
							$s=0;
							$total_nhif_days = 0;
							
							foreach ($item_invoiced_rs->result() as $key => $key_items):
								$service_charge_id = $key_items->service_charge_id;
								$service_charge_name = $key_items->service_charge_name;
								$visit_charge_amount = $key_items->visit_charge_amount;
								$service_name = $key_items->service_name;
								$units = $key_items->visit_charge_units;
								$service_id = $key_items->service_id;
								$personnel_id = $key_items->personnel_id;
								$vatable = $key_items->vatable;
								// var_dump($vatable); die();
								if($vatable)
								{
									$vat = $visit_charge_amount * $units * 0.16;
									$total_vat += $vat;
								}

								
								$doctor = '';
								
								if($service_name == 'Bed charge')
								{
									$total_nhif_days = $units;
								}
								
								if($personnel_id > 0)
								{
									$doctor_rs = $this->reception_model->get_personnel($personnel_id);
									if($doctor_rs->num_rows() > 0)
									{
										$key_personnel = $doctor_rs->row();
										$first_name = $key_personnel->personnel_fname;
										$personnel_onames = $key_personnel->personnel_onames;
										$doctor = ' : Dr. '.$personnel_onames.' '.$first_name;
									}
								}
								//var_dump($service_id);
								//if lab check to see if drug is in pres
								if($service_id == 4)
								{
									if($this->accounts_model->in_pres($service_charge_id, $visit_id))
									{
										$visit_total = $visit_charge_amount * $units;
										$visit_total = floor($visit_total);
										$s++;
										
										?>
										<tr>
											<td><?php echo $s;?></td>
											<td><?php echo $service_charge_name;?></td>
											<td><?php echo $units;?></td>
											<td><?php echo floor($visit_total);?></td>
										</tr>
										<?php
										$total = $total + $visit_total;
									}
								}
								
								else
								{
									//$debit_note_pesa = $this->accounts_model->total_debit_note_per_service($service_id,$visit_id);
									
									//$credit_note_pesa = $this->accounts_model->total_credit_note_per_service($service_id,$visit_id);
									
									$visit_total = $visit_charge_amount * $units;
									$visit_total = floor($visit_total);
									$s++;
									
									//$visit_total = ($visit_total + $debit_note_pesa) - $credit_note_pesa;
									?>
									<tr>
										<td><?php echo $s;?></td>
										<td><?php echo $service_charge_name;?></td>
										<td><?php echo $units;?></td>
										<td><?php echo $visit_charge_amount;?></td>
										<td><?php echo $vat;?></td>
										<td><?php echo floor($visit_total);?></td>
									</tr>
									<?php
									$total = $total + $visit_total;
								}
							endforeach;
							
							//less NHIF Rebate
							if(($inpatient == 1) && (!empty($patient_insurance_number)))
							{
								$rebate = 1600;
								$total_rebate = $rebate * $total_nhif_days;
								$s++;
								?>
                                <tr>
                                    <td><?php echo $s;?></td>
                                    <td colspan="2">Less NHIF Rebate</td>
                                    <td><?php echo $total_nhif_days;?></td>
                                    <td><?php echo number_format($rebate,2);?></td>
                                    <td>(<?php echo number_format($total_rebate,2);?>)</td>
                                </tr>
                                <?php
								$total = $total - $total_rebate;
							}
							$total_amount = $total ;
							
							// $total_amount = ($total + $debit_note_amount) - $credit_note_amount;
                        }
						
						$total_services = count($services_billed);
						if($total_services > 0)
						{
							for($t = 0; $t < $total_services; $t++)
							{
								$s++;
								$debit_note_pesa  = 0;
								$credit_note_pesa = 0;
								
								$payment_service_name = $services_billed[$t]['payment_service_name'];
								$payment_service_id = $services_billed[$t]['payment_service_id'];
								
								$debit_note_pesa = $this->accounts_model->total_debit_note_per_service($payment_service_id, $visit_id);
								
								$credit_note_pesa = $this->accounts_model->total_credit_note_per_service($payment_service_id, $visit_id);
								//get service name
								$service_name = $payment_service_name;
								if($debit_note_pesa > 0)
								{
									?>
									<tr>
										<td><?php echo $s;?></td>
										<td><?php echo $service_name;?></td>
										<td>Debit notes</td>
										<td>1</td>
										<td><?php echo number_format($debit_note_pesa,2);?></td>
										<td><?php echo number_format($debit_note_pesa,2);?></td>
									</tr>
									<?php
								}
								
								if($credit_note_pesa > 0)
								{
									?>
									<tr>
										<td><?php echo $s;?></td>
										<td><?php echo $service_name;?></td>
										<td>Credit notes</td>
										<td>1</td>
										<td>(<?php echo number_format($credit_note_pesa,2);?>)</td>
										<td>(<?php echo number_format($credit_note_pesa,2);?>)</td>
									</tr>
									<?php
								}
								$total_amount = ($total_amount + $debit_note_pesa) - $credit_note_pesa;
							}
						}
					  	
					
					  
					  	if(count($payments_rs) > 0)
						{
							$x=0;
							
							foreach ($payments_rs as $key_items):
								$x++;
								$payment_type = $key_items->payment_type;
								$payment_status = $key_items->payment_status;
								
								if($payment_type == 1 && $payment_status == 1)
								{
									$payment_method = $key_items->payment_method;
									$amount_paid = $key_items->amount_paid;
									
									$total_payments = $total_payments + $amount_paid;
								}
							endforeach;
						}
					  
                      ?>
                        
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row receipt_bottom_border" style="margin-top:10px;">
        	<div class="col-md-6">
        	</div>
        	<div class="col-md-6">
            	<div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">Total Amount:</div>
                        
                    	<strong> <?php echo number_format($total_amount - $total_vat ,2);?></strong>
                    </div>
                </div>

               
                <div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">Total VAT 16 %:</div>
                        
                    	<strong> <?php echo number_format($total_vat,2);?></strong>
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">Payment:</div>
                        
                    	<strong> <?php echo number_format($total_payments,2);?></strong>
                    </div>
                </div>
                 <div class="row" style="border-top:2px solid #000">
                	<div class="col-md-12">
                    	<div class="title-item">Balance:</div>                        
                    	<strong><?php echo number_format($total_amount - $total_payments,2);?></strong>
                    </div>
                </div>
            	
            </div>
        </div>
        <div class="row receipt_bottom_border" style="margin-top: 10px;">
        	<div class="center-align">
        		<h3>FISCAL RECEIPT</h3>
        		<div class="row" style="font-style:italic; font-size:10px;">
		        	<div >
		            	Served by: <?php echo $served_by; ?>
		            </div>
		        	<div >
		            	<?php echo $today; ?> Thank you
		            </div>
		        </div>
        	</div>
        	
        </div>
        
    	
    </body>
    
</html>