<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/administration/controllers/administration.php";

class Laboratory extends administration
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reception/reception_model');
		$this->load->model('laboratory_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('pharmacy/pharmacy_model');
		$this->load->model('admin/dashboard_model');
		$this->load->model('laboratory/lab_model');
	}

	public function index()
	{
		$where = 'visit.visit_type = visit_type.visit_type_id AND visit_charge.visit_id = visit.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.lab_test_id = lab_test.lab_test_id AND visit_charge.charged = 1 AND visit_lab_test.visit_lab_test_id = visit_charge.visit_lab_test_id AND visit.visit_delete = 0';
		$table = 'visit_charge,visit,visit_type,lab_test,patients,visit_lab_test';


		$visit_report_search = $this->session->userdata('laboratory_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
// var_dump($where); die();
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'hospital-reports/lab-test-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->laboratory_model->get_all_laboratory_visits($table, $where, $config["per_page"], $page, 'ASC');


		$lab_test_where = 'service_charge.service_charge_status = 1 AND service_charge.service_charge_delete = 0 AND lab_test.lab_test_delete = 0 AND service_charge.service_charge_name = lab_test.lab_test_name AND lab_test_class.lab_test_class_id = lab_test.lab_test_class_id  AND service_charge.service_id = service.service_id AND (service.service_name = "Lab" OR service.service_name = "lab" OR service.service_name = "Laboratory" OR service.service_name = "laboratory" OR service.service_name = "Laboratory test")  AND  service_charge.visit_type_id = 1';
		    
		$lab_test_table = '`service_charge`, lab_test_class, lab_test, service';
		$lab_test_order ='lab_test_name';
		$lab_test_query = $this->lab_model->get_inpatient_lab_tests($lab_test_table, $lab_test_where, $lab_test_order);

		$rs11 = $lab_test_query->result();
		$lab_tests = '';
		foreach ($rs11 as $lab_test_rs) :


		  $lab_test_id = $lab_test_rs->service_charge_id;
		  $lab_test_name = $lab_test_rs->service_charge_name;

		  $lab_test_price = $lab_test_rs->service_charge_amount;

		  $lab_tests .="<option value='".$lab_test_id."'>".$lab_test_name." KES.".$lab_test_price."</option>";

		endforeach;

		$v_data['lab_tests'] = $lab_tests;


		$v_data['visit_types'] = $this->reception_model->get_visit_types();

		$page_title = 'Laboratory Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('laboratory/test_summary_statement', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function index_old()
	{
		
		$where = 'lab_test_class.lab_test_class_id = lab_test.lab_test_class_id';
		$table = 'lab_test,lab_test_class';
		
		$lab_tests = $this->session->userdata('lab_tests_search');
		
		if(!empty($lab_tests))
		{
			$where .= $lab_tests;
		}
		else
		{
			// date todays dates
			$search =' AND visit_date = \''.date('Y-m-d').'\'';
			$search_title = 'Tests of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';
			$this->session->set_userdata('lab_tests_search_title', $search_title);
			$this->session->set_userdata('tests_report_search', $search);

		}
		
		$segment = 3;

		$order = 'lab_test_name';
		$order_method = 'ASC';
		$page_name = '__';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'hospital-reports/lab-test-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 15;
		$config['num_links'] = 4;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->laboratory_model->get_all_test_list($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$search_title = $this->session->userdata('lab_tests_search_title');
		
		if(!empty($search_title))
		{
			$title = $search_title;
		}
		
		else
		{
			$title = 'Test List';
		}
		
		$data['title'] = $title;
		$v_data['title'] = $title;
		$v_data['module'] = 0;
		
		$data['content'] = $this->load->view('laboratory/laboratory_report', $v_data, true);
		
		
		$data['sidebar'] = 'lab_sidebar';
		
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}


	public function search_lab_tests()
	{
		$lab_test_name = $this->input->post('lab_test_name');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		
		$search_title = 'Showing reports for: ';
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit_date>= \''.$visit_date_from.'\' AND visit_date<= \''.$visit_date_to.'\'';
			$search_title .= 'Date from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit_date = \''.$visit_date_from.'\'';
			$search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit_date = \''.$visit_date_to.'\'';
			$search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_date = '';
		}
		
		if(!empty($lab_test_name))
		{
			$lab_test_name = ' AND lab_test.lab_test_name LIKE \'%'.$lab_test_name.'%\' ';
		}
		
		$search = $lab_test_name;
		$lab_search =$visit_date; 
		$this->session->set_userdata('lab_tests_search', $search);
		$this->session->set_userdata('lab_visit_date_to', $visit_date_to);
		$this->session->set_userdata('tests_report_search', $lab_search);
		$this->session->set_userdata('lab_tests_search_title', $search_title);
		
		redirect('hospital-reports/lab-test-report');
	}
	public function close_test_search()
	{
		$this->session->unset_userdata('lab_tests_search');
		redirect('hospital-reports/lab-test-report');
	}	

	public function test_statement($lab_test_id)
	{
		$where = 'visit_lab_test.visit_id = visit_charge.visit_id AND visit.visit_type = visit_type.visit_type_id AND visit_lab_test.visit_id = visit.visit_id AND visit_charge.lab_test_id  = '.$lab_test_id;
		$table = 'visit_lab_test, visit_charge,visit,visit_type';


		$visit_report_search = $this->session->userdata('laboratory_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
// var_dump($where); die();
		$segment = 4;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'medical-reports/tests-report/'.$lab_test_id;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->laboratory_model->get_all_visit_all_procedures($table, $where, $config["per_page"], $page, 'ASC');


		$page_title = 'Laboratory Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('laboratory/test_statement', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	
}