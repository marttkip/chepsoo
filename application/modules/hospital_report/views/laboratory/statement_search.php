        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Search Procedures</h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("search-tests-report", array("class" => "form-horizontal"));
            ?>
            	<div class="row">
            		<div class="col-md-6">
	                    <div class="form-group">
	                        <label class="col-lg-4 control-label">Lab Test: </label>
	                        
	                        <div class="col-lg-8">
	                            <select id='lab_test_id' name='lab_test_id' class='form-control custom-select '>
								  <option value=''>None - Please Select a Lab test</option>
								  <?php echo $lab_tests;?>
								</select>
	                        </div>
	                    </div>	

	                    <div class="form-group">
	                        <label class="col-lg-4 control-label">Visit Type: </label>
	                        
	                        <div class="col-lg-8">
	                            <select name="visit_type_id" id="visit_type_id2" class="form-control">
									<option value="">----Select a visit type----</option>
									<?php
															
										if($visit_types->num_rows() > 0){

											foreach($visit_types->result() as $row):
												$visit_type_name = $row->visit_type_name;
												$visit_type_id = $row->visit_type_id;

												if($visit_type_id == set_value('visit_type_id'))
												{
													echo "<option value='".$visit_type_id."' selected='selected'>".$visit_type_name."</option>";
												}
												
												else
												{
													echo "<option value='".$visit_type_id."'>".$visit_type_name."</option>";
												}
											endforeach;
										}
									?>
								</select>
	                        </div>
	                    </div>                   
	                    
	                </div>
	                <div class="col-md-6">
	                    
	                    <div class="form-group">
	                        <label class="col-lg-4 control-label">Visit Date From: </label>
	                        
	                        <div class="col-lg-8">
	                        	<div class="input-group">
	                                <span class="input-group-addon">
	                                    <i class="fa fa-calendar"></i>
	                                </span>
	                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_from" placeholder="Visit Date From" autocomplete="off">
	                            </div>
	                        </div>
	                    </div>
	                     <div class="form-group">
	                        <label class="col-lg-4 control-label">Visit Date To: </label>
	                        
	                        <div class="col-lg-8">
	                        	<div class="input-group">
	                                <span class="input-group-addon">
	                                    <i class="fa fa-calendar"></i>
	                                </span>
	                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_to" placeholder="Visit Date To" autocomplete="off">
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                
                </div>
                <br>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                        	<div class="center-align">
                           		<button type="submit" class="btn btn-info">Search</button>
            				</div>
                        </div>
                    </div>
                </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>
		<script type="text/javascript">
				$(function() {
	    $("#lab_test_id").customselect();
	});
		</script>