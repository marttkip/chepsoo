<?php

class Laboratory_model extends CI_Model 
{


	public function get_all_test_list($table, $where, $per_page, $page, $order = 'lab_test_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		//$this->db->group_by('service_charge.lab_test_id');
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	function get_tests_done($lab_test_id)
	{
		$where = 'v_visit_lab_tests.lab_test_id = \''.$lab_test_id.'\'';
		$search = $this->session->userdata('tests_report_search');
		if(!empty($search))
		{
			$where .= $search;
		}
		$this->db->where($where);
		$this->db->from('v_visit_lab_tests');
		$this->db->select('SUM(total_tests) AS total_tests');
		//echo 'SELECT count(visit_charge_id) AS total_tests FROM service_charge, visit_charge WHERE '.$where;die();
		$query = $this->db->get();
		
		$total_tests = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$total_tests = $row->total_tests;
		}
		return $total_tests;
		
	}
	function get_tests_revenue($lab_test_id)
	{
		$where = 'visit_lab_test.visit_lab_test_id = visit_charge.visit_lab_test_id AND service_charge.service_charge_id = visit_lab_test.service_charge_id AND service_charge.lab_test_id = \''.$lab_test_id.'\'';
		//$where = 'service_charge.service_charge_id = visit_charge.service_charge_id AND service_charge.lab_test_id = \''.$lab_test_id.'\'';
		$search = $this->session->userdata('tests_report_search');
		if(!empty($search))
		{
			$where .= $search;
		}
		$this->db->where($where);
		$this->db->from('service_charge, visit_charge, visit_lab_test');
		$this->db->select('SUM(visit_charge_units * visit_charge_amount) AS total_test_revenue');
		$query = $this->db->get();
		
		$total_test_revenue = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$total_test_revenue = $row->total_test_revenue;
		}
		return $total_test_revenue;
	}
	public function get_all_laboratory_visits($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.visit_id,visit.visit_date,visit_charge.visit_charge_units,visit_charge.visit_charge_amount, patients.patient_surname,patients.patient_othernames,patients.patient_number,visit.personnel_id,visit_type.visit_type_id,visit_type.visit_type_name,patients.patient_type,lab_test.lab_test_name,visit_charge.created_by AS charged_by,visit_lab_test.created_by AS initiated_by');
		$this->db->where($where);
		$this->db->order_by('visit.visit_id','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_visit_all_procedures($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.visit_id,visit.visit_date,visit_charge.visit_charge_units,visit_charge.visit_charge_amount, patients.patient_surname,patients.patient_othernames,patients.patient_number,visit.personnel_id,visit_type.visit_type_id,visit_type.visit_type_name,patients.patient_type,visit_lab_test.created_by AS initiated_by, visit_charge.created_by AS charged_by');
		$this->db->where($where);
		$this->db->order_by('visit.visit_id','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_personnel_name($personnel_id)
	{
		$this->db->where('personnel_id',$personnel_id);
		$this->db->from('personnel');
		$this->db->select('personnel_fname,personnel_onames');
		$query = $this->db->get();
		
		$personnel_name = '';
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$personnel_name = $row->personnel_fname.' '.$row->personnel_onames;
		}

		return $personnel_name;
	}

}
?>